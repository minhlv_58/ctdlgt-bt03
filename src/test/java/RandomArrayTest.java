import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by minh on 22/09/2015.
 */
public class RandomArrayTest {
    private static long start;
    @BeforeClass
    public static void start() {
        start = System.currentTimeMillis();
    }

    @AfterClass
    public static void end() {
        System.out.print("the RandomArray take ");
        System.out.print(System.currentTimeMillis() - start);
        System.out.println("ms to compelete");
    }

    @Test
    public void testRandomize1() throws Exception {
        Assert.assertEquals(1, RandomArray.randomize(1).length);
    }

    @Test
    public void testRandomize2() throws Exception {
        Assert.assertEquals(0, RandomArray.randomize(-1).length);
    }

    @Test
    public void testRandomize3() throws Exception {
        Assert.assertEquals(99999, RandomArray.randomize(99999).length);
    }

    @Test
    public void testZeroFind1() throws Exception {
        Assert.assertTrue(RandomArray.ZeroFind(new int[]{0, 1, 2, 3, 4}));
    }

    @Test
    public void testZeroFind2() throws Exception {
        Assert.assertFalse(RandomArray.ZeroFind(new int[]{1 ,2, 3, 4, 20000}));
    }
}