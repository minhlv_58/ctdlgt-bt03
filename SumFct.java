import java.math.BigInteger;

public class SumFct {
	public static BigInteger perimeter(BigInteger n) {
		// your code
    	BigInteger a = new BigInteger("1");
    	BigInteger b = new BigInteger("1");
    	BigInteger c = new BigInteger("0");
    	BigInteger s = new BigInteger("2");
    	BigInteger di = new BigInteger("1");

    	if(n.compareTo(BigInteger.valueOf(0)) == 0){
    		return BigInteger.valueOf(0);
    	} else if (n.compareTo(BigInteger.valueOf(1)) == 0) {
    		return BigInteger.valueOf(4);
    	}

    	for(BigInteger i = new BigInteger("2"); i.compareTo(n) != 1;i = i.add(di)){
    		c = a.add(b);
    		a = b;
    		b = c;
    		s = s.add(c);

    		System.out.println(String.format("i: %s, c: %s, s: %s",i.toString(), c.toString(), s.toString()));
    	}

    	return s.multiply(BigInteger.valueOf(4));
	}

	public static void main(String[] args){
		BigInteger pra = new BigInteger(args[0]);
		System.out.println(perimeter(pra).toString());
	}
}